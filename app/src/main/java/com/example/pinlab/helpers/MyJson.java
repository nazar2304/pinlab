package com.example.pinlab.helpers;

import com.example.pinlab.App;

import java.io.IOException;
import java.io.InputStream;

public class MyJson {
    public static String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = App.getApp().getAssets().open("products.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
