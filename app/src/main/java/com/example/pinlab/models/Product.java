package com.example.pinlab.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Product {

	@SerializedName("longDescription")
	private String longDescription;

	@SerializedName("titleColor")
	private String titleColor;

	@SerializedName("isSubProduct")
	private boolean isSubProduct;

	@SerializedName("detailPhoto")
	private String detailPhoto;

	@SerializedName("background")
	private String background;

	@SerializedName("buyLink")
	private String buyLink;

	@SerializedName("catalogPhoto")
	private String catalogPhoto;

	@SerializedName("id")
	private int id;

	@SerializedName("shortDescription")
	private String shortDescription;

	@SerializedName("title")
	private String title;

	@SerializedName("subproducts")
	private List<Product> subproducts;

	@SerializedName("experiments")
	private List<Experiment> experiments;

	public void setLongDescription(String longDescription){
		this.longDescription = longDescription;
	}

	public String getLongDescription(){
		return longDescription;
	}

	public void setTitleColor(String titleColor){
		this.titleColor = titleColor;
	}

	public String getTitleColor(){
		return titleColor;
	}

	public void setDetailPhoto(String detailPhoto){
		this.detailPhoto = detailPhoto;
	}

	public String getDetailPhoto(){
		return detailPhoto;
	}

	public void setBackground(String background){
		this.background = background;
	}

	public String getBackground(){
		return background;
	}

	public void setBuyLink(String buyLink){
		this.buyLink = buyLink;
	}

	public String getBuyLink(){
		return buyLink;
	}

	public void setCatalogPhoto(String catalogPhoto){
		this.catalogPhoto = catalogPhoto;
	}

	public String getCatalogPhoto(){
		return catalogPhoto;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setShortDescription(String shortDescription){
		this.shortDescription = shortDescription;
	}

	public String getShortDescription(){
		return shortDescription;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setSubProducts(List<Product> subproducts){
		this.subproducts = subproducts;
	}

	public List<Product> getSubProducts(){
		return subproducts;
	}

	public List<Product> getSubproducts() {
		return subproducts;
	}

	public void setSubproducts(List<Product> subproducts) {
		this.subproducts = subproducts;
	}

	public List<Experiment> getExperiments() {
		return experiments;
	}

	public void setExperiments(List<Experiment> experiments) {
		this.experiments = experiments;
	}

	public boolean isSubProduct() {
		return isSubProduct;
	}

	public void setSubProduct(boolean subProduct) {
		isSubProduct = subProduct;
	}
}