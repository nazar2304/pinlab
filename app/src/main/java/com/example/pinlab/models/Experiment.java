package com.example.pinlab.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Experiment {

    private String title;
    private String icon;
    private String description;

    @SerializedName("instruction")
    private List<Instruction> instruction;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Instruction> getInstruction() {
        return instruction;
    }

    public void setInstruction(List<Instruction> instruction) {
        this.instruction = instruction;
    }
}
