package com.example.pinlab.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Products{

	@SerializedName("products")
	private List<Product> products;

	public void setProducts(List<Product> products){
		this.products = products;
	}

	public List<Product> getProducts(){
		return products;
	}
}