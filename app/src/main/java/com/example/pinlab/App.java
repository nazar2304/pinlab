package com.example.pinlab;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.example.pinlab.dagger.AppComponent;
import com.example.pinlab.dagger.DaggerAppComponent;
import com.example.pinlab.dagger.module.ContextModule;
import com.example.pinlab.models.Products;
import io.fabric.sdk.android.Fabric;


public class App extends Application {
    public static Products products;
    private static AppComponent sAppComponent;
    private static App app;
    private float dp;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        sAppComponent = DaggerAppComponent.builder().contextModule(new ContextModule(this)).build();
        app = this;
        dp = getResources().getDisplayMetrics().density;
    }

    public float getDp() {
        return dp;
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public static App getApp() {
        return app;
    }
}