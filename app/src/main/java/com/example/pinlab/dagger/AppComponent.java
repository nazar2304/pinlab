package com.example.pinlab.dagger;


import com.example.pinlab.dagger.module.ContextModule;
import com.example.pinlab.dagger.module.LocalStorageModule;
import com.example.pinlab.ui.info.InfoPresenter;
import com.example.pinlab.ui.experiments.ExperimentsPresenter;
import com.example.pinlab.ui.instruction.InstructionPresenter;
import com.example.pinlab.ui.list.ListPresenter;
import com.example.pinlab.ui.splash.SplashPresenter;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {ContextModule.class, LocalStorageModule.class})
public interface AppComponent {

    void inject(SplashPresenter example);

    void inject(ListPresenter example);

    void inject(InfoPresenter example);

    void inject(ExperimentsPresenter example);

    void inject(InstructionPresenter example);
}

