package com.example.pinlab.ui.instruction;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.pinlab.App;
import com.example.pinlab.R;
import com.example.pinlab.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.pinlab.views.ZoomView;


public class InstructionActivity extends BaseActivity implements InstructionView {

    @BindView(R.id.image) ImageView mImageView;
    @BindView(R.id.seekBar) SeekBar mSeekBar;
    @BindView(R.id.zoom) ZoomView mZoom;
    @BindView(R.id.step) TextView mStep;
    @BindView(R.id.description) TextView mDescription;
    @InjectPresenter InstructionPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);
        unbinder = ButterKnife.bind(this);
        mPresenter.onLoad();
        mSeekBar.setOnTouchListener((v, event) -> true);
    }

    @OnClick(R.id.back)
    protected void onClickBack() {
        finish();
    }

    @OnClick(R.id.right)
    protected void onClickRight(){
        mPresenter.onClickNext();

    }

    @OnClick(R.id.left)
    protected void onClickLeft(){
        mPresenter.onClickPrevious();
    }


    @Override
    public void showImage(String image) {
       mImageView.setImageResource(getResources().getIdentifier(image, "drawable", App.getApp().getPackageName()));
    }

    @Override
    public void showSeekBar(int current, int max) {
        mSeekBar.setMax(max);
        mSeekBar.setProgress(current);
        mZoom.setSmoothZoom(1.0f);
        mStep.setText("Шаг " + (current + 1) + " из " + (max + 1));
    }

    @Override
    public void showDescription(String description) {
        mDescription.setText(description);
    }
}
