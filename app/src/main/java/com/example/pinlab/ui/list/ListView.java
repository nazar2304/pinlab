package com.example.pinlab.ui.list;


import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.pinlab.base.BaseView;

public interface ListView extends BaseView {
    @StateStrategyType(SkipStrategy.class)
    void openInfoActivity();
    @StateStrategyType(SkipStrategy.class)
    void openExperimentsActivity();

    void notifyList();
}

