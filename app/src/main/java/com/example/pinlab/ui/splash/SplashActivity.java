package com.example.pinlab.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.pinlab.App;
import com.example.pinlab.base.BaseActivity;
import com.example.pinlab.helpers.MyJson;
import com.example.pinlab.models.Products;
import com.example.pinlab.ui.main.MainActivity;
import com.google.gson.Gson;

public class SplashActivity extends BaseActivity implements SplashView {
    @InjectPresenter SplashPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.openMainActivity();
    }

    @Override
    public void openMainActivity() {
        new Handler().postDelayed(() -> {
            Gson gson = new Gson();
            App.products = gson.fromJson(MyJson.loadJSONFromAsset(), Products.class);
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        }, 0);
    }
}
