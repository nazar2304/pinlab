package com.example.pinlab.ui.info;

import com.arellomobile.mvp.InjectViewState;
import com.example.pinlab.App;
import com.example.pinlab.base.BasePresenter;
import com.example.pinlab.dagger.module.LocalStorage;
import com.example.pinlab.models.Product;

import javax.inject.Inject;

@InjectViewState
public class InfoPresenter extends BasePresenter<InfoView> {

    @Inject LocalStorage mLocalStorage;
    private Product product;

    InfoPresenter() {
        App.getAppComponent().inject(this);
    }

    void load() {
        product = mLocalStorage.readObject("info", Product.class);
        getViewState().showProduct(product);
    }

    void onClickCart() {
        getViewState().openCart(product.getBuyLink());
    }
    void onClickCollect() {
        getViewState().openInstruction();
    }
}
