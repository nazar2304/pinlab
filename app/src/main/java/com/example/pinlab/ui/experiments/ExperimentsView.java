package com.example.pinlab.ui.experiments;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.pinlab.base.BaseView;

public interface ExperimentsView extends BaseView {

    void notifyList();
    void setTitle(String title);
    @StateStrategyType(SkipStrategy.class)
    void openInstruction();
}
