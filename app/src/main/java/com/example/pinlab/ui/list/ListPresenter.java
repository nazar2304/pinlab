package com.example.pinlab.ui.list;

import com.arellomobile.mvp.InjectViewState;
import com.example.pinlab.App;
import com.example.pinlab.base.BasePresenter;
import com.example.pinlab.dagger.module.LocalStorage;
import com.example.pinlab.models.Product;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class ListPresenter extends BasePresenter<ListView> {
    @Inject LocalStorage mLocalStorage;

    private List<Product> mProducts = new ArrayList<>();

    List<Product> getProducts() {
        return mProducts;
    }

    ListPresenter() {
        App.getAppComponent().inject(this);
    }

    void load(){
        for (int i = 0; i < App.products.getProducts().size(); i++){
            mProducts.add(App.products.getProducts().get(i));
            if(App.products.getProducts().get(i).getSubProducts() != null){
                mProducts.addAll(App.products.getProducts().get(i).getSubProducts());
            }
        }
        getViewState().notifyList();
    }

    void onClickInfo(Product product) {
        mLocalStorage.writeObject("info", product);
        getViewState().openInfoActivity();
    }

    void onClickCollect(Product product) {
        mLocalStorage.writeObject("info", product);
        getViewState().openExperimentsActivity();
    }
}
