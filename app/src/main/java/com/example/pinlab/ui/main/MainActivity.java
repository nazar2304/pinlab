package com.example.pinlab.ui.main;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TextView;

import com.example.pinlab.R;
import com.example.pinlab.base.BaseActivity;
import com.example.pinlab.ui.list.ListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.drawerLayout) DrawerLayout mDrawerLayout;
    @BindView(R.id.version) TextView mVersion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        changeFragment(new ListFragment(), "ListFragment");

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            mVersion.setText(pInfo.versionName);
        } catch (Exception ignored) {
        }
    }

    @OnClick(R.id.iconDrawer)
    protected void onClickIconDrawer(){
        if(mDrawerLayout.isDrawerOpen(Gravity.START)){
            mDrawerLayout.closeDrawer(Gravity.START);
        } else {
            mDrawerLayout.openDrawer( Gravity.START);
        }
    }

    @OnClick(R.id.site)
    protected void onClickSite(){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("http://pinlab.ru/"));
        startActivity(i);
    }

    @OnClick(R.id.shop)
    protected void onClickShop(){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("http://shop.pinlab.ru/"));
        startActivity(i);
    }


    public void changeFragment(Fragment fragment, String tag) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, fragment, tag).commit();
    }
}


