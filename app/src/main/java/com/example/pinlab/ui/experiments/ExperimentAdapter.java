package com.example.pinlab.ui.experiments;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pinlab.App;
import com.example.pinlab.R;
import com.example.pinlab.base.BaseRecyclerAdapter;
import com.example.pinlab.models.Experiment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExperimentAdapter extends BaseRecyclerAdapter<Experiment, ExperimentAdapter.ViewHolder> {

    private IClickListener mListener;

    ExperimentAdapter(List<Experiment> experimentList) {
        items = experimentList;
    }

    void setListener(IClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ExperimentAdapter.ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_experiment, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bindView(getItem(i));
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title) TextView mTitle;
        @BindView(R.id.description) TextView mDescription;
        @BindView(R.id.icon) ImageView mIcon;
        @BindView(R.id.container) View mContainer;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @SuppressLint("InflateParams")
        void bindView(Experiment experiment) {
            mTitle.setText(experiment.getTitle());
            mIcon.setImageResource(mIcon.getResources().getIdentifier(experiment.getIcon(), "drawable", App.getApp().getPackageName()));
            mDescription.setText(experiment.getDescription());
            mContainer.setOnClickListener(v -> mListener.onClickIcon(experiment));
        }
    }

    interface  IClickListener {
        void onClickIcon(Experiment experiment);
    }
}
