package com.example.pinlab.ui.list;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.pinlab.R;
import com.example.pinlab.base.BaseFragment;
import com.example.pinlab.models.Product;
import com.example.pinlab.ui.info.InfoActivity;
import com.example.pinlab.ui.experiments.ExperimentsActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.pinlab.App.products;


public class ListFragment extends BaseFragment implements ListView{
    @BindView(R.id.container) RecyclerView list;
    @InjectPresenter ListPresenter mPresenter;
    ListAdapter listAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        list.setLayoutManager(new LinearLayoutManager(getContext()));

        listAdapter = new ListAdapter(mPresenter.getProducts());
        list.setAdapter(listAdapter);
        listAdapter.setListener(new ListAdapter.IClickListener() {
            @Override
            public void onClickInfo(Product product) {
                mPresenter.onClickInfo(product);
            }

            @Override
            public void onClickBuy(Product product) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(product.getBuyLink()));
                startActivity(i);
            }

            @Override
            public void onClickCollect(Product product) {
               mPresenter.onClickCollect(product);
            }
        });

        mPresenter.load();
        return view;
    }


    @Override
    public void openInfoActivity() {
        startActivity(new Intent(getActivity(), InfoActivity.class));
    }

    @Override
    public void openExperimentsActivity() {
        startActivity(new Intent(getActivity(), ExperimentsActivity.class));
    }

    @Override
    public void notifyList() {
        listAdapter.notifyDataSetChanged();
    }
}
