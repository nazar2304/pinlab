package com.example.pinlab.ui.list;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.pinlab.App;
import com.example.pinlab.R;
import com.example.pinlab.base.BaseRecyclerAdapter;
import com.example.pinlab.models.Product;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListAdapter extends BaseRecyclerAdapter<Product, RecyclerView.ViewHolder> {
    private IClickListener mListener;
    private LayoutInflater inflater = (LayoutInflater) App.getApp().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    ListAdapter(List<Product> productsItems) {
        items = productsItems;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if(i == 0){
            return new ProductViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_product, viewGroup, false));
        } else {
            return new ViewHolderTitle(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_title, viewGroup, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if(viewHolder instanceof ProductViewHolder){
            ((ProductViewHolder)viewHolder).bindView(getItem(i));
        } else {
            ((ViewHolderTitle)viewHolder).bindView(getItem(i));
        }
    }

    class ViewHolderTitle extends RecyclerView.ViewHolder {

        @BindView(R.id.title) TextView title;
        @BindView(R.id.info) View info;
        @BindView(R.id.container) View container;
        @BindView(R.id.separator) View separator;

        ViewHolderTitle(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindView(Product product){
            title.setText(product.getTitle());
            title.setTextColor(Color.parseColor(product.getTitleColor()));
            info.setOnClickListener(v -> mListener.onClickInfo(product));
            container.setBackgroundColor(Color.parseColor(product.getBackground()));
            separator.setVisibility(product.isSubProduct() ? View.GONE : View.VISIBLE);
        }
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.shortDescription) TextView shortDescription;
        @BindView(R.id.title) TextView title;
        @BindView(R.id.container) View container;
        @BindView(R.id.separator) View separator;
        @BindView(R.id.photo) ImageView photo;
        @BindView(R.id.info) View info;
        @BindView(R.id.collect) View collect;
        @BindView(R.id.cart) View cart;


        ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @SuppressLint("InflateParams")
        void bindView(Product product) {
            shortDescription.setText(product.getShortDescription());
            title.setText(product.getTitle());
            title.setTextColor(Color.parseColor(product.getTitleColor()));
            container.setBackgroundColor(Color.parseColor(product.getBackground()));
            separator.setVisibility(product.isSubProduct() ? View.GONE : View.VISIBLE);
            Glide.with(photo).load(photo.getResources().getIdentifier(product.getCatalogPhoto(), "drawable", App.getApp().getPackageName())).into(photo);

            info.setOnClickListener(v -> mListener.onClickInfo(product));
            collect.setOnClickListener(v -> mListener.onClickCollect(product));
            collect.setVisibility(product.getExperiments() != null ? View.VISIBLE : View.GONE);
            cart.setOnClickListener(v -> mListener.onClickBuy(product));
        }
    }

    void setListener(IClickListener listener) {
        mListener = listener;
    }

    interface IClickListener {
        void onClickInfo(Product product);
        void onClickBuy(Product product);
        void onClickCollect(Product product);
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getSubProducts() == null ? 0 : 1;
    }
}
