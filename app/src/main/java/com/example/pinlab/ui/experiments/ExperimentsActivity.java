package com.example.pinlab.ui.experiments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.pinlab.R;
import com.example.pinlab.base.BaseActivity;
import com.example.pinlab.ui.instruction.InstructionActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExperimentsActivity extends BaseActivity implements ExperimentsView {

    @BindView(R.id.list) RecyclerView list;
    @BindView(R.id.title) TextView mTitle;
    @InjectPresenter ExperimentsPresenter mPresenter;
    private ExperimentAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experiments);
        unbinder = ButterKnife.bind(this);

        list.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ExperimentAdapter(mPresenter.getExperiments());
        list.setAdapter(mAdapter);
        mAdapter.setListener(experiment -> mPresenter.onClickExperiment(experiment));

        mPresenter.load();
    }

    @OnClick(R.id.back)
    protected void onClickBack() {
        finish();
    }

    @Override
    public void notifyList() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setTitle(String title) {
        mTitle.setText(title);
    }

    @Override
    public void openInstruction() {
        startActivity(new Intent(this, InstructionActivity.class));
    }
}
