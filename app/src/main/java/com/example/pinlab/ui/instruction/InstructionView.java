package com.example.pinlab.ui.instruction;

import com.example.pinlab.base.BaseView;

public interface InstructionView extends BaseView {
    void showImage(String image);
    void showSeekBar(int current, int max);
    void showDescription(String description);
}
