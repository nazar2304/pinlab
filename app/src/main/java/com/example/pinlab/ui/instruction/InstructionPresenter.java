package com.example.pinlab.ui.instruction;

import com.arellomobile.mvp.InjectViewState;
import com.example.pinlab.App;
import com.example.pinlab.base.BasePresenter;
import com.example.pinlab.dagger.module.LocalStorage;
import com.example.pinlab.models.Experiment;

import javax.inject.Inject;

@InjectViewState
public class InstructionPresenter extends BasePresenter<InstructionView> {

    @Inject
    LocalStorage mLocalStorage;
    private Experiment experiment;
    private int id = -1;

    InstructionPresenter() {
        App.getAppComponent().inject(this);
        experiment = mLocalStorage.readObject("experiment", Experiment.class);
    }

    void onClickNext() {
        if (id >= experiment.getInstruction().size() - 1)
            return;
        id++;
        showCurrentImage();
    }

    void onClickPrevious() {
        if (id == 0)
            return;
        id--;
        showCurrentImage();
    }

    private void showCurrentImage() {
        getViewState().showImage(experiment.getInstruction().get(id).getIcon());
        getViewState().showSeekBar(id, experiment.getInstruction().size() - 1);
        getViewState().showDescription(experiment.getInstruction().get(id).getText());
    }

    void onLoad() {
        if(id == -1){
            onClickNext();
        }
    }
}
