package com.example.pinlab.ui.splash;

import com.example.pinlab.base.BaseView;

public interface SplashView extends BaseView {
    void openMainActivity();
}

