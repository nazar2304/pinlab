package com.example.pinlab.ui.info;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.pinlab.base.BaseView;
import com.example.pinlab.models.Product;

public interface InfoView extends BaseView {
    void showProduct(Product product);
    void openCart(String url);
    @StateStrategyType(SkipStrategy.class)
    void openInstruction();
}
