package com.example.pinlab.ui.experiments;

import com.arellomobile.mvp.InjectViewState;
import com.example.pinlab.App;
import com.example.pinlab.base.BasePresenter;
import com.example.pinlab.dagger.module.LocalStorage;
import com.example.pinlab.models.Experiment;
import com.example.pinlab.models.Product;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class ExperimentsPresenter extends BasePresenter<ExperimentsView> {

    @Inject LocalStorage mLocalStorage;
    private Product product;
    private List<Experiment> mExperiments = new ArrayList<>();

    ExperimentsPresenter() {
        App.getAppComponent().inject(this);
        product = mLocalStorage.readObject("info", Product.class);
    }

    List<Experiment> getExperiments() {
        return mExperiments;
    }

    void load() {
        mExperiments.clear();
        mExperiments.addAll(product.getExperiments());
        getViewState().setTitle(product.getTitle());
        getViewState().notifyList();
    }

    void onClickExperiment(Experiment experiment) {
        mLocalStorage.writeObject("experiment", experiment);
        getViewState().openInstruction();
    }
}
