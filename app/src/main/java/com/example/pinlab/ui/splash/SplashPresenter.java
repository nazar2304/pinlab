package com.example.pinlab.ui.splash;

import com.arellomobile.mvp.InjectViewState;
import com.example.pinlab.App;
import com.example.pinlab.base.BasePresenter;
import com.example.pinlab.dagger.module.LocalStorage;

import javax.inject.Inject;

@InjectViewState
public class SplashPresenter extends BasePresenter<SplashView> {
    @Inject LocalStorage mLocalStorage;

    SplashPresenter() {
        App.getAppComponent().inject(this);
    }

    void openMainActivity(){
        getViewState().openMainActivity();
    }
}
