package com.example.pinlab.ui.info;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.pinlab.App;
import com.example.pinlab.R;
import com.example.pinlab.base.BaseActivity;
import com.example.pinlab.models.Product;
import com.example.pinlab.ui.experiments.ExperimentsActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.deanwild.flowtextview.FlowTextView;

public class InfoActivity extends BaseActivity implements InfoView {

    @InjectPresenter InfoPresenter mPresenter;
    @BindView(R.id.iconMain) ImageView iconMain;
    @BindView(R.id.iconSecond) ImageView iconSecond;
    @BindView(R.id.description) FlowTextView description;
    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.collect) View mCollect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        unbinder = ButterKnife.bind(this);
        mPresenter.load();
    }

    @OnClick(R.id.back)
    protected void onClickBack() {
        finish();
    }

    @Override
    public void showProduct(Product product) {
        description.setText(product.getLongDescription());
        description.setTextSize(14 * App.getApp().getDp());
        description.setTypeface(Typeface.create("sans-serif-condensed-light", Typeface.NORMAL));
        iconMain.setImageResource(getResources().getIdentifier(product.getCatalogPhoto(), "drawable", App.getApp().getPackageName()));
        iconSecond.setImageResource(getResources().getIdentifier(product.getDetailPhoto(), "drawable", App.getApp().getPackageName()));
        mTitle.setText(product.getTitle());
        mCollect.setVisibility(product.getExperiments() != null ? View.VISIBLE : View.GONE);
    }

    @Override
    public void openCart(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void openInstruction() {
        startActivity(new Intent(this, ExperimentsActivity.class));
    }

    @OnClick(R.id.cart)
    protected void onClickCart() {
        mPresenter.onClickCart();
    }

    @OnClick(R.id.collect)
    protected void onClickCollect() {
        mPresenter.onClickCollect();
    }
}
